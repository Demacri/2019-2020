# Diario lezioni

Nota bene: a lezione non è detto si riesca ad affrontare tutti gli argomenti, ma per l'esame vanno conosciuti i capitoli del libro elencati nel README.

## Lezioni

### Future

Le date "future" cambiano (riprogrammazione in funzione di eventi vari) man mano che andiamo avanti col corso.
Non sono in perfetto ordine di presentazione.

* mem (EPROM, + F macro, PROGMEM, mappa mem, esptool?, avrdude?)
* (STUDENTI) esercizio bottone e interrupt (rimbalzi): stampa qualcosa letto da un sensore e deve contemporaneamente reagire al pulsante e fare qualcosa d'altro

* rete, protocolli, MQTT

* (STUDENTI) tanti esercizi sensori (farsi dire cosa hanno a disposizione)
* (STUDENTI) esercizio servo

* TaskScheduler e multitasking cooperativo

* PWM "dei poveri", PWM "vera"

* PID

### Passate

* 2020-04-29 (v) motori, servo e stepper
* 2020-04-22 (v) oscilloscopio, LA, bitbanging "visto", interrupt
* 2020-04-22 (v) pullup e pulldown, i/o, bitbanging
* 2020-04-17 (v) intervento Carraturo
* 2020-04-15 (v) conclusione elettronica, panoramica sensori e attuatori
* 2020-04-10 (v) condensatore, esperimento Arduino curva di carica e scarica, ronzatore, trasformatore, intro FanLoop
* 2020-04-08 (v) condensatori, RC, proseguimento panoramica componenti, PWM
* 2020-04-03 (v) partitore resistivo e potenziometro, lettura da Arduino
* 2020-04-01 (v) Kirchhoff, ac/dc, forme d'onda, serie (manca parallelo), voltmetro e amperometro, partitore
* 2020-03-27 (v) primi passi GPIO, breadboard, led, resistenze
* 2020-03-25 (v) legge di Ohm, resistenze
* 2020-03-20 (v) primi passi IDE
* 2020-03-18 (v) intro sistemi embedded, fino a "conversione ad/da escluso"
* 2020-03-13 (v) intro corso, prove tecniche video conf

## Argomenti da evadere (non in ordine di presentazione, non riusciremo a trattare tutti i topic in aula)

FIXME cfr. file lista domande
