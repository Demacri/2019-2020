//#define SENSORE 4  // sulla board Arduino UNO solo 2,3
#define SENSORE 2
#define ATTUATORE 12

// TODO 'volatile' (vedere se si può creare un esperimento ad-hoc, non banale)

volatile long timestamp=0;
volatile long durata=0;
volatile boolean updated=false;

//boolean fai=true; // per il "monitor"

void setup() {
    Serial.begin(115200);
    pinMode(SENSORE,INPUT); // c'è qualcosa che non va? ;)
    pinMode(ATTUATORE,OUTPUT);

    // CONFIGURAZIONE
    attachInterrupt(
        digitalPinToInterrupt(SENSORE),
        rispostaInterrupt,
        RISING);
}

void loop() {
    //boolean scattato=digitalRead(SENSORE); // non serve più
    if(updated) {
        //Serial.print(timestamp);
        //Serial.print(F(","));
        Serial.println(durata);
        //Serial.print(",");
        //Serial.println(scattato);
        updated=false;
    }
    // a questo punto posso permettermi tranquillamente di "dormire" anche a lungo... o no?
    delay(500);
    digitalWrite(ATTUATORE,HIGH);
    delay(500);
    digitalWrite(ATTUATORE,LOW);

    /* versione "grande"
    	LOW  800
    	HIGH 250 non meno, se no non fa nemmeno in tempo a partire
    	minima durata circa 20ms, max circa 60

       versione "piccola", ad esempio
        LOW  500
        HIGH 500
    */

}

void rispostaInterrupt() {
    //noInterrupts(); // non si può invocare da dentro, non ha effetto, va usata fuori per marcare sezioni critiche, qui dentro parte già a interrupt disabilitati

    /* versione "monitor" (dei poveri)
    if (fai) {
    	fai=false;
    	durata=millis()-timestamp;
    	timestamp=millis();
    	//scattato=true;
    	fai=true;
    }
    */

    //Serial.println("int!"); //da non fare...

    long m=millis();
    if((m-timestamp)>5) { // per eliminare gli interrupt spuri
        durata=m-timestamp;
        timestamp=m;
        updated=true;
    }

    //interrupts(); // questa invece si può invocare, mah! ;) (e in automatico vengono cmq riabilitati all'uscita)
}
