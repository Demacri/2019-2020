#define PINOUT 7
#define READFR A0

#define SOGLIAL 300
//#define SOGLIAH 390 //così non oscilla (di sera)
#define SOGLIAH 500 //così oscilla
// indipendentemente dall'isteresi l'oscillazione è solo un tema di soglie

#define ISTERESI

void setup() {
  pinMode(PINOUT, OUTPUT);
  Serial.begin(115200);
}

void loop() {
  int sensorValue = analogRead(READFR);

  /* togliendo print e/p delay... ??? */
  Serial.println(sensorValue);
  //delay(50);

  twilight(PINOUT, sensorValue);
}

void twilight(int pin, int sv) {
  if (sv < SOGLIAL) // se luminosità bassa allora mi accendo
    digitalWrite(pin, HIGH);
  else
#ifdef ISTERESI
  if (sv > SOGLIAH) // mi spengo solo se oltrepassa una soglia distante dalla bassa
#endif
    digitalWrite(pin, LOW);
}
