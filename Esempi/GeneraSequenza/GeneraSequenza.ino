/*
 * genera sequenza (inventata di sana pianta) per logical analyzer
 * 
 * si può applicare il LA ai piedini RX, TX e PIN per vedere cosa passa,
 * anche cambiando il sorgente per sperimentare
 */

#define PIN LED_BUILTIN

void setup() {
    Serial.begin(115200);
    pinMode(PIN,OUTPUT);
}

int ch=32;
void loop() {
    if(ch>127) {
        ch=32;
        Serial.print(0); // questo non c'è mai altrimenti

        // "marker" sul pin
        digitalWrite(PIN,LOW);
        delay(50);
        digitalWrite(PIN,HIGH);
        delay(50);
        digitalWrite(PIN,LOW);
        delay(50);
    } else {
        ch++;
    }

    if(ch%2==0) {
        digitalWrite(PIN,LOW);
        //Serial.println("0");
    } else {
        digitalWrite(PIN,HIGH);
        //Serial.println(F("1"));
    }

    Serial.print(ch);
    delay(1);
}
